package com.example.contactlistapp;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ContactListActivity extends ListActivity {

	boolean isDeleting = false;
	ContactAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_list);
		initListButton();
        initMapButton();
        initSettingsButton();
        initDeleteButton();
        initAddContactButton();


        BroadcastReceiver batteryReceiver = new BroadcastReceiver() { // 1

        @Override
        public void onReceive( Context context, Intent intent) {
            double batteryLevel = intent.getIntExtra( BatteryManager.EXTRA_LEVEL, 0); // 2
            double levelScale = intent.getIntExtra( BatteryManager.EXTRA_SCALE, 0); // 3
            int batteryPercent = (int) Math.floor( batteryLevel/ levelScale* 100); // 4
            TextView textBatteryState =(TextView) findViewById( R.id.textBatteryLevel);
            textBatteryState.setText( batteryPercent +"%");
            }
        };
        IntentFilter filter = new IntentFilter( Intent.ACTION_BATTERY_CHANGED); // 5
        registerReceiver( batteryReceiver, filter);

        
        /*
        String sortBy = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortfield", "contactname"); 
        String sortOrder = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortorder", "ASC");
        ContactDataSource ds = new ContactDataSource(this);
        ds.open();
        //ArrayList<String> names = ds.getContactName();
        //final ArrayList<Contact> contacts = ds.getContacts();
        final ArrayList < Contact > contacts = ds.getContacts( sortBy, sortOrder);
        ds.close();
        
        //setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names));
        //setListAdapter(new ContactAdapter(this, contacts));
        adapter = new ContactAdapter( this, contacts);
        setListAdapter( adapter);
        
        ListView listView = getListView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id){
        		Contact selectedContact = contacts.get( position);
        		if (isDeleting){
        			adapter.showDelete( position, itemClicked, ContactListActivity.this, selectedContact);
        		}
        		else{
            		Intent intent = new Intent(ContactListActivity.this, ContactActivity.class);
            		intent.putExtra("contactid", selectedContact.getContactID());
            		startActivity(intent);        			
        		}
        	}
		});
		*/
	}
	
	@Override public void onResume() { 
		super.onResume(); 
		String sortBy = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortfield", "contactname"); 
		String sortOrder = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortorder", "ASC"); 
		ContactDataSource ds = new ContactDataSource( this); 
		ds.open(); 
		final ArrayList < Contact > contacts = ds.getContacts( sortBy, sortOrder); 
		ds.close(); 
		
		//if (contacts.size() > 0) { // 1 
			adapter = new ContactAdapter( this, contacts); 
			setListAdapter(adapter);
			ListView listView = getListView(); 
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
					Contact selectedContact = contacts.get(position);
					if (isDeleting) {
						adapter.showDelete(position, itemClicked, ContactListActivity.this, selectedContact);
					} else {
						Intent intent = new Intent(ContactListActivity.this, ContactActivity.class);
						intent.putExtra("contactid", selectedContact.getContactID());
						startActivity(intent);
					}
				}
			});
			/*} 
		else { 
			Intent intent = new Intent( ContactListActivity.this, ContactActivity.class); 
			startActivity( intent); 
			}	*/	
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contact_list, menu);
		return true;
	}
	
	private void initListButton() {
        ImageButton list = (ImageButton) findViewById(R.id.imageButtonList);
        list.setEnabled(false);
//        list.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//    			Intent intent = new Intent(ContactListActivity.this, ContactListActivity.class);
//    			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//    			startActivity(intent);
//            }
//        });
	}
	/*
	private void initMapButton() {
        ImageButton list = (ImageButton) findViewById(R.id.imageButtonMap);
        list.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(ContactListActivity.this, ContactMapsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}
	*/


    private void initMapButton() {
        ImageButton list = (ImageButton) findViewById(R.id.imageButtonMap);
        list.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String sortBy = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortfield", "contactname");
                String sortOrder = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortorder", "ASC");
                ContactDataSource ds = new ContactDataSource(getBaseContext());
                ds.open();
                final ArrayList< Contact > contacts = ds.getContacts( sortBy, sortOrder);
                ds.close();

                Intent intent = new Intent(ContactListActivity.this, ContactMapsActivity.class);
                if (contacts== null  || contacts.size() == 0) {
                    Toast.makeText(getBaseContext(), "Contact must be saved before it can be mapped", Toast.LENGTH_LONG).show();
                }
                else {
                    intent.putExtra("contactid", contacts.get(0).getContactID());
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    /*
    private void initMapButton() {
		ImageButton list = (ImageButton) findViewById(R.id.imageButtonMap);
		list.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(ContactListActivity.this, ContactMapsActivity.class);
				String sortBy = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortfield", "contactname");
				String sortOrder = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortorder", "ASC");
				ContactDataSource ds = new ContactDataSource(ContactListActivity.this);
				ds.open();
				final ArrayList < Contact > contacts = ds.getContacts(sortBy, sortOrder);
				ds.close();
				intent.putExtra("contacts", contacts);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}
	*/

	private void initSettingsButton() {
        ImageButton list = (ImageButton) findViewById(R.id.imageButtonSettings);
        list.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
    			Intent intent = new Intent(ContactListActivity.this, ContactSettingsActivity.class);
    			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    			startActivity(intent);
            }
        });
	}
	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initDeleteButton() { 
		final Button deleteButton = (Button) findViewById( R.id.deleteButton);
		deleteButton.setOnClickListener( new OnClickListener() { 
			public void onClick( View v) { 
				if (isDeleting) { 
					deleteButton.setText("Delete");
					isDeleting = false; // 1 
					adapter.notifyDataSetChanged(); 
					} 
				else { 
					deleteButton.setText("Done Deleting"); 
					isDeleting = true; 
					} 
				} 
			}); 
		}
				
	private void initAddContactButton() { 
		Button newContact = (Button) findViewById( R.id.addButton); 
		newContact.setOnClickListener( new OnClickListener() { 
			public void onClick( View v) { 
				Intent intent = new Intent( ContactListActivity.this, ContactActivity.class); 
				startActivity( intent); 
				} 
			}); 
		}
	
}
