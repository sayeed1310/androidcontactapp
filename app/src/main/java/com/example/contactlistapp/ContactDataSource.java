package com.example.contactlistapp;

import android.database.Cursor;
//import java.sql.SQLException;
import android.database.SQLException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.Time;

public class ContactDataSource {
	private SQLiteDatabase database; // 1
	private ContactDBHelper dbHelper;
	public ContactDataSource( Context context) { // 2
		dbHelper = new ContactDBHelper( context); 
		}
	
	public void open() throws SQLException { // 3
		database = dbHelper.getWritableDatabase(); 
		}
	
	public void close() { // 4
		dbHelper.close();
		}
	
	public boolean insertContact( Contact c) { 
		boolean didSucceed = false; // 1 
		try { ContentValues initialValues = new ContentValues(); // 2 
		initialValues.put(" contactname", c.getContactName()); // 3 
		initialValues.put(" streetaddress", c.getContactAddress().getStreetAddress()); 
		initialValues.put(" city", c.getContactAddress().getCity()); 
		initialValues.put(" state", c.getContactAddress().getState()); 
		initialValues.put(" zipcode", c.getContactAddress().getZipCode()); 
		initialValues.put(" phonenumber", c.getPhoneNumber()); 
		initialValues.put(" cellnumber", c.getCellNumber()); 
		initialValues.put(" email", c.getEMail());
		initialValues.put(" birthday", String.valueOf( c.getBirthday(). toMillis( false)));

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		c.getPicture(). compress( Bitmap.CompressFormat.PNG, 100, baos);
		byte[] photo = baos.toByteArray();
		initialValues.put(" contactphoto", photo);

			//initialValues.put(" bff", c.getBff());
		didSucceed = database.insert(" contact", null, initialValues) > 0; // 4 
		}
		catch (Exception e) { // Do nothing -will return false if there is an exception // 5 
			} 
		return didSucceed; 
		}

	public boolean updateContact( Contact c) { 
		boolean didSucceed = false; 
		try { 
			Long rowId = Long.valueOf( c.getContactID()); // 6 
			ContentValues updateValues = new ContentValues(); 
			updateValues.put(" contactname", c.getContactName());
			updateContactAddress(c.getContactAddress(),rowId);
			//updateValues.put(" streetaddress", c.getStreetAddress()); 
			//updateValues.put(" city", c.getCity()); 
			//updateValues.put(" state", c.getState()); 
			//updateValues.put(" zipcode", c.getZipCode()); 
			updateValues.put(" phonenumber", c.getPhoneNumber()); 
			updateValues.put(" cellnumber", c.getCellNumber()); 
			updateValues.put(" email", c.getEMail());
			updateValues.put(" birthday", String.valueOf(c.getBirthday().toMillis(false)));

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			c.getPicture(). compress(Bitmap.CompressFormat.PNG, 100, baos);
			byte[] photo = baos.toByteArray();
			updateValues.put(" contactphoto", photo);

			//updateValues.put(" bff", c.getBff());
			didSucceed = database.update(" contact", updateValues, "_id =" + rowId, null) > 0; // 7 
			} catch (Exception e) { // Do nothing -will return false if there is an exception 
				}
		return didSucceed;
		}
	
	public int getLastContactId() {
		int lastId = -1;
		try {
			String query = "Select MAX(_id) from contact"; // 1
			Cursor cursor = database.rawQuery( query, null); // 2
			
			cursor.moveToFirst(); // 3
			lastId = cursor.getInt( 0); // 4
			cursor.close(); // 5
			}
		catch (Exception e) { 
			lastId = -1; 
			}
		return lastId;
		}
		
	public boolean updateContactAddress ( ContactAddress c, Long rowId ) {
		boolean didSucceed = false; 
		try { 
			//Long rowId = Long.valueOf( c.getContactID()); // 6 
			ContentValues updateValues = new ContentValues(); 
			updateValues.put(" streetaddress", c.getStreetAddress()); 
			updateValues.put(" city", c.getCity()); 
			updateValues.put(" state", c.getState()); 
			updateValues.put(" zipcode", c.getZipCode()); 
			didSucceed = database.update(" contact", updateValues, "_id =" + rowId, null) > 0; // 7 
			} catch (Exception e) { // Do nothing -will return false if there is an exception 
				}
		return didSucceed;
	}
	
	public ArrayList < String > getContactName() {
		ArrayList < String > contactNames = new ArrayList < String >(); // 1 
		try { 
			String query = "Select contactname from contact"; // 2 
			Cursor cursor = database.rawQuery( query, null); 
			cursor.moveToFirst(); // 3 
			while (! cursor.isAfterLast()) { 
				contactNames.add( cursor.getString( 0)); 
				cursor.moveToNext(); 
				} 
			cursor.close(); 
			} 
		catch (Exception e) { 
			contactNames = new ArrayList < String >(); // 4 
			} 
		return contactNames; 
		}
		
	//public ArrayList<Contact> getContacts() {
	public ArrayList<Contact> getContacts(String sortField, String sortOrder) {
		ArrayList<Contact> contacts = new ArrayList<Contact>();
		try{
			//String query = "Select * from contact";
			String query = "SELECT * FROM contact ORDER BY " + sortField + " " + sortOrder;
		
			Cursor cursor = database.rawQuery(query, null);
			
			Contact newContact;
			cursor.moveToFirst();
			while(!cursor.isAfterLast()){
				newContact = new Contact();
				newContact.setContactID(cursor.getInt(0));
				newContact.setContactName(cursor.getString(1));
				newContact.getContactAddress().setStreetAddress(cursor.getString(2));
				newContact.getContactAddress().setCity(cursor.getString(3));
				newContact.getContactAddress().setState(cursor.getString(4));
				newContact.getContactAddress().setZipCode(cursor.getString(5));
				newContact.setPhoneNumber(cursor.getString(6));
				newContact.setCellNumber(cursor.getString(7));
				newContact.setEMail(cursor.getString(8));
				Time t = new Time();
				t.set(Long.valueOf(cursor.getString(9)));
				newContact.setBirthday(t);

                if (cursor.getBlob(10)!= null) {
                    byte[] photo = cursor.getBlob(10);
                    if (photo != null) {
                        ByteArrayInputStream imageStream = new ByteArrayInputStream( photo);
                        Bitmap thePicture = BitmapFactory.decodeStream(imageStream);
                        newContact.setPicture( thePicture);
                    }
                }

                //newContact.setBff(cursor.getString(10));
				contacts.add(newContact);
				cursor.moveToNext();
			}
			cursor.close();
		}
		catch (Exception e){
			contacts = new ArrayList<Contact>();
		}
		return contacts;
	}
	
	public boolean deleteContact(int contactId) {
		boolean didDelete = false;
		try{
			didDelete = database.delete("contact", "_id=" + contactId, null) > 0;
		}
		catch (Exception e) {
			
		}
		return didDelete;
	}
	
	public Contact getSpecificContact( int contactId) { // 1 
		Contact contact = new Contact(); 
		String query = "SELECT * FROM contact WHERE _id =" + contactId; // 2 
		Cursor cursor = database.rawQuery( query, null); 
		if (cursor.moveToFirst()) { // 3 
			contact.setContactID( cursor.getInt( 0)); 
			contact.setContactName( cursor.getString( 1)); 
			contact.getContactAddress().setStreetAddress( cursor.getString( 2)); 
			contact.getContactAddress().setCity( cursor.getString( 3)); 
			contact.getContactAddress().setState( cursor.getString( 4)); 
			contact.getContactAddress().setZipCode( cursor.getString( 5)); 
			contact.setPhoneNumber( cursor.getString( 6)); 
			contact.setCellNumber( cursor.getString( 7)); 
			contact.setEMail( cursor.getString( 8)); 
			Time t = new Time(); 
			t.set( Long.valueOf( cursor.getString( 9))); 
			contact.setBirthday( t);

            if (cursor.getBlob(10)!= null) {
                byte[] photo = cursor.getBlob(10);
                if (photo != null) {
                    ByteArrayInputStream imageStream = new ByteArrayInputStream( photo);
                    Bitmap thePicture = BitmapFactory.decodeStream(imageStream);
                    contact.setPicture( thePicture);
                }
            }

			cursor.close(); 
			} 
		return contact; 
	}
		
}

